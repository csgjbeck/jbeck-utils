window.onload = function jbeck_form_filler() {

	// TODO: input form id
	var formId = "aspnetForm";

	/* Region Random String */
	
	function randomString(length) {
		return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
	}
	
    /* Region Activate */

    function activate() {
		var inputs = $("form#" + formId + " :input:text"),
			selects = $("form#" + formId + " select"),
			radios = $("form#" + formId + " :input:radio"),
			textAreas = $("form#" + formId + " :input:text, textarea");		
		
		inputs.each(function () {
			$(this).val(randomString(8));
		});
		selects.each(function () {
			var optionVal = $(this).find("option:eq(0)").val();
			if(optionVal === "") optionVal = $(this).find("option:eq(1)").val();
			$(this).val(optionVal);
		});
		radios.each(function() {
			$(this).prop('checked', true);
		});
		textAreas.each(function() {
			$(this).val(randomString(24));
		});	
    }

    activate();
}