window.onload = function jbeck_form_filler() {

    /* Region Members */

    var inputs = [
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff2_1_ctl00_ctl00_TextField",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff3_1_ctl00_ctl00_TextField",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff6_1_ctl00_ctl00_TextField",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff7_1_ctl00_ctl00_TextField",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff9_1_ctl00_ctl00_TextField",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff60_1_ctl00_ctl00_TextField"
	],
	selects = [
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff8_1_ctl00_DropDownChoice"
	],
	radios = [
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff37_1_ctl00_ctl00",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff38_1_ctl00_ctl00",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff39_1_ctl00_ctl00",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff40_1_ctl00_ctl00",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff41_1_ctl00_ctl00",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff43_1_ctl00_ctl00"
	],
	textAreas = [
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff58_1_ctl00_ctl00_TextField",
		"#ctl00_ctl42_g_a495f48c_90bb_40ae_a2bc_84d1318cb4a8_ff59_1_ctl00_ctl00_TextField"
	];

	/* Region Random String */
	
	function randomString(length) {
		return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
	}
	
    /* Region Activate */

    function activate() {
		inputs.forEach(function (item) {
			$(item).val(randomString(8));
		});
		selects.forEach(function (item) {
			var optionVal = $(item + " option:eq(0)").val();
			if(optionVal === "") optionVal = $(item + " option:eq(1)").val();
			$(item).val(optionVal);
		});
		radios.forEach(function(item) {
			$(item).prop('checked', true);
		});
		textAreas.forEach(function(item) {
			$(item).val(randomString(24));
		});	
    }

    activate();
}